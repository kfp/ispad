#!/bin/bash

#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
# __author__ = "Kurt Pagani <nilqed@gmail.com>"
# __svn_id__ = "$Id: setup 2 2015-10-02 01:56:19Z pagani $"
#
# Create ispad kernel.json (spec)  in ~/.ipython/kernels/ispad
#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

# QuickLisp 
if sbcl --non-interactive --load "quick.lisp" ; then
  echo SBCL/QuickLisp ......... ok. ; else
  echo Error: SBCL/QuickLisp failed. See README. ; exit ;
fi 


# Get installation directory
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


# Create kernel spec
if [ -d $DIR/ispad ]; then
  rm -r $DIR/ispad ;
fi

if mkdir $DIR/ispad; then
  kspec=$DIR/ispad/"kernel.json" ; else
  echo Error: Cannot create kernel spec directory. ; exit ;
fi
  
echo '{"argv": ['"\"$DIR/ispad.sh\""',"{connection_file}"],' > $kspec
echo '"codemirror_mode": "shell",' >> $kspec
echo '"display_name": "iSPAD",' >> $kspec
echo '"language": "spad"}' >> $kspec
echo kernel.json written to $kspec
echo

# Install kernel spec
if jupyter kernelspec install --user $DIR/ispad ; then
  echo Kernel spec installation ....... ok. ; else
  echo Error: Kernel spec installation failed. ; exit ;
fi 


echo
echo "Now run: jupyter notebook" 
echo "Point your browser to http://localhost:8888"
echo "Choose kernel: New -> iSPAD"
echo