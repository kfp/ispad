#!/bin/bash

#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
# __author__ = "Kurt Pagani <nilqed@gmail.com>"
# __svn_id__ = "$Id: ispad.sh 3 2015-10-02 03:10:41Z pagani $"
# 
# Start the kernel with parameter "{connection_file}" from kernel.json
#;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


# get installation directory
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


# Lisp files to load
ql=$DIR/quick.lisp
il=$DIR/ispad.lisp


# prepare and load commands for ispad
sk="(cl-jupyter::kernel-start \"$1\")"
ldql="(load \"$ql\")"
ldil="(load \"$il\")"



# start a spad client (fricas in this case)
fricas -nox -eval ")lisp $ldql" -eval ")lisp $ldil" -eval ")lisp $sk"
            





 