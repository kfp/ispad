## iSPAD - Jupyter kernel for FriCAS/Axiom/OpenAxiom in Common Lisp

Thanks to Frederic Peschanski's [`cl-jupyter`](https://github.com/fredokun/cl-jupyter) 
it is now easy to develop a native kernel for the **Axiom** computer algebra system 
(and forks).

Implemented so far (Version 0.9.7):

* Normal (algebraic) input/output
* MathJax rendering (set output tex on)
* Command completion (press TAB)
* Function introspection (press Shift-TAB)
* Error handling (red background, try 1/0)
* Plotting by `GnuDraw` (inserting via html)  
* Message signing (by updating cl-jupyter)
* CodeMirror mode/ pygments lexer
* Display/insert `base64:png` on: `Type: FileName`
* Displays now `svg`, `html`, `json`, `js`, `md` as well
* History management (partly)
* Display/rendering from within SPAD (see `meta/jupyter.spad`) 
* GLE interface
* Triggering type 'String' if starting with a prefix (def: $HTMl$)


This `README` is actually exported from a *Jupyter* notebook session as *markdown*. 

Recent [Snapshot (V 0.9.2)](http://kfp.bitbucket.org/tmp/version-0-9-2.html) exported 
as `HTML`.

There is now a `Docker` [image: ispad](https://hub.docker.com/r/nilqed/ispad/) 
and new (independent, auto-build) `Dockerfiles`:

* [fricas_docker](https://github.com/nilqed/fricas_docker)
* [ispad_docker](https://github.com/nilqed/ispad_docker)


**Note:** This project is under development and not intended for production yet.
For `FriCAS/SBCL` there is a `core`version:
[fricas_jupyter](https://github.com/nilqed/fricas_jupyter)


##### Prerequisites

`iSPAD` requires *QuickLisp*, *Jupyter* and *FriCAS* or *OpenAxiom*. Which versions are 
exactly required is not yet known, however, the following has been tested on Ubuntu 14, 
whereby *FriCAS* was compiled using the latest *SBCL* [(1.2.16)](http://www.sbcl.org/) 
version.

* [QuickLisp](https://www.quicklisp.org/beta/)
* [Jupyter 4.0](https://jupyter.org/) or [later](http://jupyter.org/)
* [FriCAS 1.2.x](http://fricas.sourceforge.net/)

**Note:** It is assumed here that *QuickLisp* is installed in `~/quicklisp`. 
Otherwise you have to adjust the path in the source file `quick.lisp`. 

Update (V 0.9.3): Upgrade/Install Jupyter by

    pip install -U jupyter

Former versions (ipython) are outdated.


##### Installation

Get this repository:

    git clone https://bitbucket.org/kfp/ispad.git


Change to the folder `ispad` and run the setup script:

    ./setup.sh
    
**Note**: whenever the folder `ispad` is moved you must run the setup 
script again (to update the path in kernel.json). If there are errors
open an issue.


##### Running 

Start the Jupyter notebook:

    jupyter  notebook 

Then point your browser to 

    http://localhost:8888
    
Press the `New` tab to the right and choose the `iSPAD` kernel. What then happens can 
be seen in the log further below. Since this is a development version the loading might 
take some seconds (Lisp sources). Wait until the kernel is ready.

If you want access the kernel from another machine add `--ip=**` to the `jupyter` start
command and be aware that this is highly insecure outside a private network.


##### Session Log

See [Notebook](http://kfp.bitbucket.org/tmp/ispad_test.html) output (outdated!).


    kfp@helix:~/IPyNB$ jupyter notebook --ip=*
    [W 04:02:07.791 NotebookApp] WARNING: The notebook server is listening on all IP addresses and not using encryption. This is not recommended.
    [W 04:02:07.792 NotebookApp] WARNING: The notebook server is listening on all IP addresses and not using authentication. This is highly insecure and not recommended.
    [I 04:02:07.803 NotebookApp] Serving notebooks from local directory: /home/kfp/IPyNB
    [I 04:02:07.803 NotebookApp] 0 active kernels
    [I 04:02:07.803 NotebookApp] The IPython Notebook is running at: http://[all ip addresses on your system]:8888/
    [I 04:02:07.803 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
    [W 04:02:07.804 NotebookApp] No web browser found: could not locate runnable browser.
    [W 04:02:08.691 NotebookApp] 404 GET /api/kernels/c42b3331-7be3-402e-a4b2-bf3d29d2f675/channels?session_id=9D8C2F97F6334BC48405378BF3867798 (192.168.0.6): Kernel does not exist: c42b3331-7be3-402e-a4b2-bf3d29d2f675
    [W 04:02:08.766 NotebookApp] 404 GET /api/kernels/c42b3331-7be3-402e-a4b2-bf3d29d2f675/channels?session_id=9D8C2F97F6334BC48405378BF3867798 (192.168.0.6) 92.44ms referer=None
    [W 04:02:12.811 NotebookApp] 404 GET /api/kernels/c42b3331-7be3-402e-a4b2-bf3d29d2f675/channels?session_id=9D8C2F97F6334BC48405378BF3867798 (192.168.0.6): Kernel does not exist: c42b3331-7be3-402e-a4b2-bf3d29d2f675
    [W 04:02:12.814 NotebookApp] 404 GET /api/kernels/c42b3331-7be3-402e-a4b2-bf3d29d2f675/channels?session_id=9D8C2F97F6334BC48405378BF3867798 (192.168.0.6) 9.78ms referer=None
    [I 04:02:16.003 NotebookApp] Kernel started: 1893f442-ec7f-4f64-a8f2-6556be225bd0
    viewman not present, disabling graphics
    clef trying to get the initial terminal settings: Inappropriate ioctl for device
    Checking for foreign routines
    AXIOM="/usr/local/lib/fricas/target/x86_64-unknown-linux"
    spad-lib="/usr/local/lib/fricas/target/x86_64-unknown-linux/lib/libspad.so"
    foreign routines found
    openServer result 0
                           FriCAS Computer Algebra System
                             Version: FriCAS 2014-12-18
                      Timestamp: Fre Okt 23 00:48:14 CEST 2015
    -----------------------------------------------------------------------------
       Issue )copyright to view copyright notices.
       Issue )summary for a summary of useful system commands.
       Issue )quit to leave FriCAS and return to shell.
    -----------------------------------------------------------------------------
    
    To load "pzmq":
      Load 1 ASDF system:
        pzmq
    ; Loading "pzmq"
    
    To load "bordeaux-threads":
      Load 1 ASDF system:
        bordeaux-threads
    ; Loading "bordeaux-threads"
    
    To load "uuid":
      Load 1 ASDF system:
        uuid
    ; Loading "uuid"
    ......
    To load "cl-base64":
      Load 1 ASDF system:
        cl-base64
    ; Loading "cl-base64"
    
    Value = T
    STYLE-WARNING:
       Implicitly creating new generic function CL-JUPYTER::WIRE-SERIALIZE.
    Value = T
    
    iSPAD V0.9.4 :: 24-OCT-2015, Jupyter kernel based on
    cl-jupyter: an enhanced interactive Common Lisp REPL
    (Version 0.7 - Jupyter protocol v.5.0)
    --> (C) 2014-2015 Frederic Peschanski (cf. LICENSE)
    
    [Heartbeat] thread started [Kernel] Entering mainloop ...
    [Heartbeat] thread started [Kernel] Entering mainloop ...
    
    [Shell] loop started
    [Recv]: issue with UTF-8 decoding
    [W 04:02:26.195 NotebookApp] Timeout waiting for kernel_info reply from 1893f442-ec7f-4f64-a8f2-6556be225bd0
    [I 04:03:01.266 NotebookApp] Saving file at /version-0-9-4.ipynb
 
    
##### Stand-alone Image

Using `FriCAS` and `SBCL` it is possible to create a stand-alone 
executable (or core image) using `FriCAS` as a CL library. There is,
however, a "malformed message" issue at the moment.

Update 0.9.3: The `mkimage.sh` script in the subfolder `img`creates
an **executable** `iSPAD` which expects a `kernel-xxxx.json` connect file as
argument. This will probably be the final solution to install the kernel. 

For FriCAS/SBCL we recommend:

* OS=Ubuntu https://github.com/nilqed/fricas_jupyter
* OS=else https://hub.docker.com/r/nilqed/fricas_jupyter/



##### CodeMirror and Pygments Lexer
In the folder `meta` you will find a CodeMirror mode for SPAD (spad.js)
contributed by Bill Page and a lexer file `spad.py` (under construction). 
Those files have to be properly placed and initialized depending on your 
I/Python installation.   

##### Interface: jupyter.spad (under construction)
The package `jupyter` contains some functions allowing plotting and displaying
strings in various formats (HTML, Markdown, LaTeX, Javascript). The forementioned
types as well as some image formats (png, svg) can also be displayed by 
coercing the the corresponding `file` to type `FileName`.

For examples see [here](http://kfp.bitbucket.org/tmp/jupyter_spad_test.html)

##### Interface: gle.spad (under construction)
[GLE](http://glx.sourceforge.net/)  (Graphics Layout Engine) is a graphics scripting language designed for 
creating publication quality figures (e.g., a chart, plot, graph, or diagram).   

For examples see [here](http://kfp.bitbucket.org/tmp/GLE.html)


##### iSPAD for SMC (SageMathCloud)
See [fricas_smc](https://github.com/nilqed/fricas_smc).


##### Revision history

Current version: 0.9.7

Find details in the header  of `ispad.lisp`.

Outlook: the `core` version is stable and allows reasonable interrupts/restarts,
so a release version (1.0) may be expected by the end of January 2016. Unfortunately
it is still necessary to have SBCL to build the image. Several attempts using
`(SPAD-SAVE <image-file> nil)` or `AxiomSYS` itself have failed. 

