(load "../quick.lisp")
(load "../ispad.lisp")
(in-package #:cl-jupyter-user)


(defun get-argv1 ()
  (cdr sb-ext:*posix-argv*))


(defun ispad-main ()
  (let ((connect-file (car (last (get-argv1)))))
     (when (probe-file connect-file)
       (princ connect-file)
       (boot::fricas-init)
       (boot::|parseAndEvalToString| ")version")
       (cl-jupyter::kernel-start connect-file)))) 


(sb-ext:save-lisp-and-die "iSPAD" :toplevel #'cl-jupyter-user::ispad-main 
                                  :executable t 
                                  :save-runtime-options t)
  

