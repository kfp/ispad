#!/bin/bash

export AXSYS="?"

if !(sbcl --version) ; then
        echo SBCL missing! Exit. 
	exit ; 
fi


if fricas -nogo > axsys ; then
	export AXSYS=$(awk '/-ws/ {print $(NF)}' axsys);
        rm axsys ;
fi

if [ -f  $AXSYS ] ; then
        echo ")lisp (load \"img.lisp\")" > acmd
	sbcl --core  $AXSYS < acmd;
	rm acmd;

else    
        echo creating iSPAD image failed.
	exit;
fi
